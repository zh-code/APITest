﻿using System;
using System.Collections.Generic;
using System.Linq;
using apitest.Model;
using apitest.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace apitest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class CompanyController : ControllerBase
    {
        private ApiTestService service = new ApiTestService();

        /// <summary>
        /// Add a new company
        /// </summary>
        /// <param name="CompanyName">Company Name</param>
        /// <returns></returns>
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(409)]
        [HttpPut("Create/{CompanyName}")]
        public IActionResult Create(string CompanyName)
        {
            IEnumerable<CompanyEntity> companies = service.CompanyRepo.GetAll();

            //Check if company exists
            CompanyEntity Company = companies.FirstOrDefault(
                o => string.Equals(o.Name, CompanyName, StringComparison.OrdinalIgnoreCase));
            
            if (Company == null)
            {
                //Create new company object
                Company = new CompanyEntity
                {
                    Name = CompanyName
                };

                service.CompanyRepo.Create(Company);

                return Ok();
            }
            //409 Conflict
            return StatusCode(409);
        }

        /// <summary>
        /// Get All Companies
        /// </summary>
        /// <returns></returns>
        [ProducesResponseType(200)]
        [AllowAnonymous]
        [HttpGet]
        public IActionResult Get()
        {
            return Ok(service.CompanyRepo.GetAll());
        }

        /// <summary>
        /// Select specific company according to Id
        /// </summary>
        /// <param name="CompanyId">Company ID</param>
        /// <returns></returns>
        [AllowAnonymous]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [HttpGet("Get/{CompanyId}")]
        public IActionResult Get(string CompanyId)
        {
            //Get company by primary key
            CompanyEntity company = service.CompanyRepo.Get(CompanyId);

            if (company != null)
            {
                return Ok(company);
            }
            return NotFound();
        }

        /// <summary>
        /// Update company name according to company Id
        /// </summary>
        /// <param name="CompanyId">Company ID</param>
        /// <param name="CompanyName">New Company Name</param>
        /// <returns></returns>
        [HttpPatch("{CompanyId}/Update/{CompanyName}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public IActionResult Update(string CompanyId, string CompanyName)
        {
            //Get target company
            IEnumerable<CompanyEntity> companies = service.CompanyRepo.GetAll();
            CompanyEntity company = companies.FirstOrDefault(o => o.Id == CompanyId);

            if (company != null)
            {
                company.Name = CompanyName;

                service.CompanyRepo.Update(company);

                return Ok();
            }

            return NotFound();
        }

        /// <summary>
        /// Delete Specified Company
        /// </summary>
        /// <param name="CompanyId">Company ID</param>
        /// <returns></returns>
        [HttpDelete("Delete/{CompanyId}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public IActionResult Delete(string CompanyId)
        {
            IEnumerable<CompanyEntity> companies = service.CompanyRepo.GetAll();
            CompanyEntity company = companies.FirstOrDefault(o => o.Id == CompanyId);

            if (company != null)
            {
                return Ok("Company found but not allowed to delete");
            }

            return NotFound();
        }

        /// <summary>
        /// Rise Exception
        /// </summary>
        [AllowAnonymous]
        [HttpGet("~/Exception")]
        public void RiseException()
        {
            throw new Exception("This is an exception");
        }
    }
}