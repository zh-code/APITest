FROM microsoft/dotnet:2.1-aspnetcore-runtime AS base
WORKDIR /app
EXPOSE 52988
EXPOSE 44356

FROM microsoft/dotnet:2.1-sdk AS build
WORKDIR /src
COPY ["apitest/apitest.csproj", "apitest/"]
RUN dotnet restore "apitest/apitest.csproj"
COPY . .
WORKDIR "/src/apitest"
RUN dotnet build "apitest.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet publish "apitest.csproj" -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "apitest.dll"]