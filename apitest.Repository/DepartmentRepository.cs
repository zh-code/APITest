﻿using apitest.Data;
using apitest.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace apitest.Repository
{
    public class DepartmentRepository : IRepository<DepartmentEntity>
    {
        private TestContext Context;

        public DepartmentRepository(TestContext _Context)
        {
            Context = _Context;
        }

        public void Create(DepartmentEntity entity)
        {
            Context.Departments.Add(entity);
            Context.SaveChanges();
        }

        public void Delete(DepartmentEntity entity)
        {
            IEnumerable<DepartmentEntity> departments = GetAll();

            if (departments.Contains(entity))
            {
                Context.Departments.Remove(entity);
                Context.SaveChanges();
            }
            else
            {
                throw new NullReferenceException();
            }
        }

        public bool Exists(DepartmentEntity entity)
        {
            IEnumerable<DepartmentEntity> departments = GetAll();
            return departments.Contains(entity);
        }

        public DepartmentEntity Get(string Id)
        {
            return Context.Departments.Find(Id);
        }

        public IEnumerable<DepartmentEntity> GetAll()
        {
            return Context.Departments.ToList();
        }

        public void Load(ref DepartmentEntity entity)
        {
            Context.Entry(entity).Collection(c => c.Employees).Load();
        }

        public void LoadAll(ref DepartmentEntity entity)
        {
            Context.Entry(entity).Collection(c => c.Employees).Load();
            Context.Entry(entity).Reference(r => r.Company).Load();
        }

        public void Update(DepartmentEntity entity)
        {
            Context.Departments.Update(entity);
            Context.SaveChanges();
        }
    }
}
