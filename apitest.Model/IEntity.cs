﻿namespace apitest.Model
{
    public interface IEntity
    {
        string Id { get; set; }
    }
}
