﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using apitest.Model;
using apitest.Repository;
using apitest.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace apitest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class DepartmentController : ControllerBase
    {
        private const string Value = "Department Not Found";

        //Private Entity Context
        private ApiTestService service = new ApiTestService();

        /// <summary>
        /// Add New Department Into Company
        /// </summary>
        /// <param name="CompanyId">Company ID</param>
        /// <param name="DepartmentName">New Department Name</param>
        /// <returns></returns>
        [HttpPut("{CompanyId}/Create/{DepartmentName}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(409)]
        public IActionResult Create(string CompanyId, string DepartmentName)
        {
            //Check if company exists
            CompanyEntity Company = service.CompanyRepo.Get(CompanyId);

            if (Company != null)
            {
                service.CompanyRepo.Load(ref Company);
                
                //Check department exist or not
                DepartmentEntity department = Company.Departments.FirstOrDefault(o => string.Equals(o.Name, DepartmentName, StringComparison.OrdinalIgnoreCase));

                if (department == null)
                {
                    department = new DepartmentEntity
                    {
                        Name = DepartmentName,
                        CompanyId = Company.Id
                    };

                    service.DepartmentRepo.Create(department);

                    return Ok();
                }
                //409 Conflict
                return StatusCode(409);
            }
            else
            {
                return NotFound("Company Not Found");
            }
        }

        /// <summary>
        /// Get all departments in all companies
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [ProducesResponseType(200)]
        [HttpGet]
        public IActionResult GetAll()
        {
            //Get all companies
            return Ok(service.DepartmentRepo.GetAll());
        }

        /// <summary>
        /// Get all departments in a company
        /// </summary>
        /// <param name="CompanyId">Company ID</param>
        /// <returns></returns>
        [AllowAnonymous]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [HttpGet("{CompanyId}/Get")]
        public IActionResult GetAllByCompany(string CompanyId)
        {
            CompanyEntity Company = service.CompanyRepo.Get(CompanyId);
            service.CompanyRepo.Load(ref Company);
            
            if (Company != null)
            {
                return Ok(Company.Departments.ToList());
            }
            else
            {
                return NotFound("Company Not Found");
            }
        }

        /// <summary>
        /// Get single department entity
        /// </summary>
        /// <param name="DepartmentId">Department ID</param>
        /// <returns></returns>
        [AllowAnonymous]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [HttpGet("Get/{DepartmentId}")]
        public IActionResult Get(string DepartmentId)
        {
            DepartmentEntity department = service.DepartmentRepo.Get(DepartmentId);

            if (department == null)
            {
                return NotFound(Value);
            }
            return Ok(department);
        }

        /// <summary>
        /// Get target department from company
        /// </summary>
        /// <param name="CompanyId">Company ID</param>
        /// <param name="DepartmentId">Department ID</param>
        /// <returns></returns>
        [AllowAnonymous]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [HttpGet("{CompanyId}/Get/{DepartmentId}")]
        public IActionResult GetByCompany(string CompanyId, string DepartmentId)
        {
            CompanyEntity Company = service.CompanyRepo.Get(CompanyId);

            if (Company != null)
            {
                service.CompanyRepo.Load(ref Company);

                //Check department exist or not
                DepartmentEntity department = Company.Departments.FirstOrDefault(o => o.Id == DepartmentId);

                if (department != null)
                {
                    return Ok(department);
                }
                return NotFound("Department Not Found");
            }
            else
            {
                return NotFound("Company Not Found");
            }
        }

        /// <summary>
        /// Update company name according to company Id
        /// </summary>
        /// <param name="DepartmentId">Department ID</param>
        /// <param name="DepartmentName">Department Name</param>
        /// <returns></returns>
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [HttpPatch("{DepartmentId}/Update/{DepartmentName}")]
        public IActionResult Update(string DepartmentId, string DepartmentName)
        {
            //Check department exist or not
            DepartmentEntity department = service.DepartmentRepo.Get(DepartmentId);

            if (department != null)
            {
                department.Name = DepartmentName;

                service.DepartmentRepo.Update(department);

                return Ok(department);
            }
            return NotFound("Department Not Found");
        }

        /// <summary>
        /// Delete Specified Department
        /// </summary>
        /// <param name="DepartmentId">Department ID</param>
        /// <returns></returns>
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [HttpDelete("Delete/{DepartmentId}")]
        public IActionResult Delete(string DepartmentId)
        {
            DepartmentEntity department = service.DepartmentRepo.Get(DepartmentId);

            if (department != null)
            {
                return Ok("Department found but not allowed to delete");
            }

            return NotFound();
        }
        
    }
}
