﻿using apitest.Data;
using apitest.Repository;

namespace apitest.Service
{
    public class ApiTestService
    {
        public readonly CompanyRepository CompanyRepo;
        public readonly DepartmentRepository DepartmentRepo;
        public readonly EmployeeRepository EmployeeRepo;

        public ApiTestService()
        {
            TestContext Context = new TestContext();
            CompanyRepo = new CompanyRepository(Context);
            DepartmentRepo = new DepartmentRepository(Context);
            EmployeeRepo = new EmployeeRepository(Context);
        }
    }
}
