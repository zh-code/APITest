﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace apitest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenciationController : ControllerBase
    {
        /// <summary>
        /// Issue JWT
        /// </summary>
        /// <param name="User"></param>
        /// <remarks>
        /// Username = Test
        /// Password = ABCD1234
        /// </remarks>
        /// <returns></returns>
        [HttpPost("Auth")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        public IActionResult Login([FromBody]UserModel User)
        {
            if (User == null)
            {
                return BadRequest("No User Specified");
            }

            if (User.Username == "Test" && User.Password == "ABCD1234")
            {
                SymmetricSecurityKey secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("asiodjfuqi3hfpijfio23u94u120934i0asoifjajsdfqwuefo"));
                SigningCredentials signingCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);
                JwtSecurityToken tokenOptions = new JwtSecurityToken(
                    issuer: "https://test.zh-code.com",
                    audience: "https://test.zh-code.com",
                    claims: new List<Claim>(),
                    expires: DateTime.Now.AddDays(1),
                    signingCredentials: signingCredentials
                    );
                string token = new JwtSecurityTokenHandler().WriteToken(tokenOptions);
                return Ok(new { Token = token });
            }


            return Unauthorized();
        }
        
    }


    public class UserModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}