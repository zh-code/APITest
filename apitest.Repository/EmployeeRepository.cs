﻿using apitest.Data;
using apitest.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace apitest.Repository
{
    public class EmployeeRepository : IRepository<EmployeeEntity>
    {
        private TestContext Context;

        public EmployeeRepository(TestContext _Context)
        {
            Context = _Context;
        }

        public void Create(EmployeeEntity entity)
        {
            IEnumerable<EmployeeEntity> e = Context.Employees.Where(o => o.Id == "").Take(2);

            e.Where(o => o.Id == "");

            Context.Employees.Add(entity);
            Context.SaveChanges();
        }

        public void Delete(EmployeeEntity entity)
        {
            IEnumerable<EmployeeEntity> Employees = GetAll();

            if (Employees.Contains(entity))
            {
                Context.Employees.Remove(entity);
                Context.SaveChanges();
            }
            else
            {
                throw new NullReferenceException();
            }
        }

        public bool Exists(EmployeeEntity entity)
        {
            IEnumerable<EmployeeEntity> employees = GetAll();
            return employees.Contains(entity);
        }

        public EmployeeEntity Get(string Id)
        {
            return Context.Employees.Find(Id);
        }

        public IEnumerable<EmployeeEntity> GetAll()
        {
            return Context.Employees.ToList();
        }

        public void Load(ref EmployeeEntity entity)
        {
            throw new NotImplementedException();
        }

        public void LoadAll(ref EmployeeEntity entity)
        {
            Context.Entry(entity).Reference(c => c.Department).Load();
            Context.Entry(entity).Reference(c => c.Department.Company).Load();
        }

        public void Update(EmployeeEntity entity)
        {
            Context.Employees.Update(entity);
            Context.SaveChanges();
        }
    }
}
