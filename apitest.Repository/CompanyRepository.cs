﻿using apitest.Data;
using apitest.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace apitest.Repository
{
    public class CompanyRepository : IRepository<CompanyEntity>
    {
        /// <summary>
        /// Database Context
        /// </summary>
        private TestContext Context;

        public CompanyRepository(TestContext _Context)
        {
            Context = _Context;
        }

        /// <summary>
        /// Create new Company
        /// </summary>
        /// <param name="entity">New Company Entity</param>
        public void Create(CompanyEntity entity)
        {
            Context.Add(entity);
            Context.SaveChanges();
        }

        /// <summary>
        /// Delete existing entity
        /// </summary>
        /// <param name="entity">Company Entity</param>
        public void Delete(CompanyEntity entity)
        {
            IEnumerable<CompanyEntity> companies = GetAll();

            if (companies.Contains(entity))
            {
                Context.Companies.Remove(entity);
                Context.SaveChanges();
            }
            else
            {
                throw new NullReferenceException();
            }
        }

        public bool Exists(CompanyEntity entity)
        {
            IEnumerable<CompanyEntity> companies = GetAll();
            return companies.Contains(entity);
        }

        /// <summary>
        /// Get company entity by id
        /// </summary>
        /// <param name="Id">Company Id</param>
        /// <returns></returns>
        public CompanyEntity Get(string Id)
        {
            return Context.Companies.Find(Id);
        }

        /// <summary>
        /// Get all companies
        /// </summary>
        /// <returns></returns>
        public IEnumerable<CompanyEntity> GetAll()
        {
            return Context.Companies.ToList();
        }

        /// <summary>
        /// Load department from company entity
        /// </summary>
        /// <param name="entity">Company ref</param>
        public void Load(ref CompanyEntity entity)
        {
            Context.Entry(entity).Collection(c => c.Departments).Load();
        }

        public void LoadAll(ref CompanyEntity entity)
        {
            Context.Entry(entity).Collection(c => c.Departments).Load();
            foreach (DepartmentEntity d in entity.Departments.ToList())
            {
                Context.Entry(d).Collection(c => c.Employees).Load();
            }
        }

        /// <summary>
        /// Update existing entity
        /// </summary>
        /// <param name="entity">Company entity</param>
        public void Update(CompanyEntity entity)
        {
            Context.Companies.Update(entity);
            Context.SaveChanges();
        }
    }
}
