﻿using apitest.Model;
using System.Collections.Generic;

namespace apitest.Repository
{
    public interface IRepository<T> where T : IEntity
    {

        /// <summary>
        /// Create
        /// </summary>
        /// <param name="entity">T</param>
        void Create(T entity);

        /// <summary>
        /// Get all in T
        /// </summary>
        /// <returns></returns>
        IEnumerable<T> GetAll();

        /// <summary>
        /// Get single by Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        T Get(string Id);

        /// <summary>
        /// Check if object exist
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool Exists(T entity);

        /// <summary>
        /// Load ICollection
        /// </summary>
        /// <param name="entity">T ref</param>
        void Load(ref T entity);

        /// <summary>
        /// Load all collections and references
        /// </summary>
        void LoadAll(ref T entity);

        /// <summary>
        /// Update existing entity
        /// </summary>
        /// <param name="entity">T</param>
        void Update(T entity);

        /// <summary>
        /// Delete existing entity
        /// </summary>
        /// <param name="entity">T</param>
        void Delete(T entity);
    }
}
