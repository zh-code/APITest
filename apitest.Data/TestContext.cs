﻿using apitest.Model;
using Microsoft.EntityFrameworkCore;

namespace apitest.Data
{

    public class TestContext: DbContext
    {
        public DbSet<EmployeeEntity> Employees { get; set; }
        public DbSet<DepartmentEntity> Departments { get; set; }
        public DbSet<CompanyEntity> Companies { get; set; }

        public TestContext() { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseSqlServer("Server=sql.zh-code.com;Database=TestDB;Integrated Security=False;User ID=TestService;Password=abcdABCD1234");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }

    }
}
