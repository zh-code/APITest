﻿using System.Linq;
using System.Threading.Tasks;
using apitest.Model;
using apitest.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace apitest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class EmployeeController : ControllerBase
    {
        private ApiTestService service = new ApiTestService();

        /// <summary>
        /// Create new employee in a department in a company
        /// </summary>
        /// <param name="DepartmentId">Department Id</param>
        /// <param name="FirstName">New User First Name</param>
        /// <param name="LastName">New User Last Name</param>
        /// <returns></returns>
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [HttpPut("{DepartmentId}/Create/{FirstName}/{LastName}")]
        public IActionResult Create(string DepartmentId, string FirstName, string LastName)
        {
            DepartmentEntity department = service.DepartmentRepo.Get(DepartmentId);

            if (department != null)
            {
                EmployeeEntity employee = new EmployeeEntity { FirstName = FirstName, LastName = LastName, Department = department };

                service.EmployeeRepo.Create(employee);

                return Ok();
            }

            return NotFound("Department Not Found");
        }

        /// <summary>
        /// Return all employee
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [ProducesResponseType(200)]
        [HttpGet("GetAll")]
        public IActionResult GetAll()
        {
            return Ok(service.EmployeeRepo.GetAll());
        }

        /// <summary>
        /// Get single employee by employee id
        /// </summary>
        /// <param name="EmployeeId">Employee ID</param>
        /// <returns></returns>
        [AllowAnonymous]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [HttpGet("Get/{EmployeeId}")]
        public IActionResult Get(string EmployeeId)
        {
            EmployeeEntity employee = service.EmployeeRepo.Get(EmployeeId);

            if (employee != null)
            {
                return Ok(employee);
            }
            return NotFound();
        }

        /// <summary>
        /// Get all employees in one company
        /// </summary>
        /// <param name="CompanyId">Company ID</param>
        /// <returns></returns>
        [AllowAnonymous]
        [ProducesResponseType(200)]
        [HttpGet("{CompanyId}/Get")]
        public IActionResult GetByCompany(string CompanyId)
        {
            CompanyEntity company = service.CompanyRepo.Get(CompanyId);

            if (company != null)
            {
                service.CompanyRepo.LoadAll(ref company);
            }

            return Ok(company.Departments);
        }

        /// <summary>
        /// Get all employees in a department in a company
        /// </summary>
        /// <param name="CompanyId">Company ID</param>
        /// <param name="DepartmentId">Department ID</param>
        /// <returns></returns>
        [AllowAnonymous]
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [HttpGet("{CompanyId}/{DepartmentId}/Get")]
        public IActionResult GetByCompanyDepartment(string CompanyId, string DepartmentId)
        {
            CompanyEntity company = service.CompanyRepo.Get(CompanyId);

            if (company != null)
            {
                service.CompanyRepo.Load(ref company);

                DepartmentEntity department = company.Departments.FirstOrDefault(o => o.Id == DepartmentId);
                if (department != null)
                {
                    service.DepartmentRepo.Load(ref department);
                    return Ok(department.Employees);
                }
                else
                {
                    return NotFound("Department Not Found");
                }
            }
            return NotFound("Company Not Found");
        }

        /// <summary>
        /// Update the existing method 
        /// </summary>
        /// <param name="EmployeeId">Employee ID</param>
        /// <param name="FirstName">New Firstname</param>
        /// <param name="LastName">New Lastname</param>
        /// <returns></returns>
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [HttpPut("Update/{EmployeeId}/{FirstName}/{LastName}")]
        public IActionResult Update(string EmployeeId, string FirstName, string LastName)
        {
            EmployeeEntity employee = service.EmployeeRepo.Get(EmployeeId);

            if (employee != null)
            {
                employee.FirstName = FirstName;
                employee.LastName = LastName;
                service.EmployeeRepo.Update(employee);
                return Ok(employee);
            }

            return NotFound("Employee Not Found");
        }

        /// <summary>
        /// Delete employee
        /// </summary>
        /// <param name="EmployeeId">Employee ID to delete</param>
        /// <returns></returns>
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [HttpDelete("Delete/{EmployeeId}")]
        public IActionResult Delete(string EmployeeId)
        {
            EmployeeEntity employee = service.EmployeeRepo.Get(EmployeeId);

            if (employee != null)
            {
                return Ok("Employee found but not allowed to delete");
            }

            return NotFound("Employee Not Found");
        }
    }
}